using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 5;
    
    Rigidbody2D rigidBody;
    


    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        SetVelocity(speed, new Vector2(Random.Range(-100, 100), Random.Range(-100, 100)));
    }

    void SetVelocity(float _speed, Vector2 dir) {
        dir = dir.normalized;
        rigidBody.velocity = _speed*dir;
    }
}
